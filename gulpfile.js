var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    notify = require("gulp-notify"),
    browserSync = require('browser-sync'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    reload = browserSync.reload;

gulp.task('styles', function() {
  return gulp.src('sass/**/*.scss')
    .pipe(sourcemaps.init())
        .pipe(sass({
            onError: notify.onError({
                wait: true,
                sound: true})
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(minifycss())
        .pipe(concat('styles.min.css'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dist'))
    .pipe(reload({stream:true}));
});

gulp.task('browser-sync', function() {
    browserSync({
        open: false,
        logPrefix: "SAD",
        tunnel: false,
        notify: false,
        server: {
            baseDir: ".",
            directory: true
        }
    });
});

gulp.task('bs-reload', function (){
    browserSync.reload();
});

 gulp.task('default', ['styles', 'browser-sync'], function(){
    gulp.watch('sass/**/*.scss', ['styles']),
    gulp.watch('**/*.html', ['bs-reload']);
 });
